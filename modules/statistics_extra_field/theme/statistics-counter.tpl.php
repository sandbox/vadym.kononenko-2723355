<?php

/**
 * @file statistics-counter.tpl.php
 * Template implementation to display the value of a statistics counter field.
 *
 * Available variables:
 * - $label: Statistics counter label.
 * - $value: Statistics counter value.
 * - $attributes: Attributes string.
 * - $classes: Classes string.
 *
 * @see template_preprocess_piwik_stats_field()
 */
?>
  <div class="statistics-counter <?php print $classes; ?>" <?php print $attributes; ?>>
    <label class="statistics-counter-label"><?php print $label; ?>:&nbsp;</label>
    <span class="statistics-counter-value"><?php print render($value); ?></span>
  </div>
